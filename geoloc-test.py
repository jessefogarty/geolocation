import urllib.request
import urllib.parse
import urllib.error
import json
import googlemaps
import ssl
import resource
# Catch SSL Errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

# Set Google Maps API Key
key = 'AIzaSyBKSoryYs7ksS-cWdArXy-ysQUdOCik_z0'

#service url
serviceurl = 'https://maps.googleapis.com/maps/api/geocode/json?'

print('')
print('>> Find Your Full Address')
street = input('Please enter your street address: ')
city = input('Please enter your city: ')
address = street + ', ' + city

# create query url (serviceurl + address)
url = serviceurl + urllib.parse.urlencode(
    {'address': address}) + '&' + urllib.parse.urlencode({'key': key})
# Test print url query
print('Retrieving URL:', url)
print('...')
print('Proccessing & Extracting Your Information')
print('...')
print('')
# send query to Google maps
raw = urllib.request.urlopen(url).read().decode()
# process json string
data = json.loads(raw)
# print json imported for reivew
# print(json.dumps(data, indent=4))

# print results
print('>>> RESULT(S) FOUND <<<')
print('')
print('Your Full address: ')
print(data['results'][0]['formatted_address'])
print('')
print('Your Coordinates')
print('Latitude: ', data['results'][0]['geometry']['location']['lat'],
      'Longitutde: ', data['results'][0]['geometry']['location']['lng'])
print('')
print('Location Type: ', data['results'][0]['types'][0])
print('')
# .getrusage() will get in bytes
# / 2 ** 20 will convert bytes in MBs
usageMB = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 2 ** 20
print('Resources used: ', usageMB, 'MB')
